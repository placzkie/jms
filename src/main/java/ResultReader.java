import javax.jms.*;
import javax.naming.NamingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Leszek Placzkiewicz on 2016-04-08.
 */
public class ResultReader {
    private final JMSInitializer jmsInitializer;

    private TopicConnectionFactory topicConnectionFactory;

    private List<Topic> solutionsTopics;
    private TopicConnection topicConnection;
    private TopicSession topicSession;

    private List<TopicSubscriber> topicSubscribers;

    public ResultReader() throws NamingException, JMSException {
        jmsInitializer = JMSInitializer.getInstance();
        initializeAdministrativeObjects();
        initializeJmsClientObjects();
    }

    private void initializeAdministrativeObjects() throws NamingException {
        topicConnectionFactory = jmsInitializer.getTopicConnectionFactory();
        solutionsTopics = jmsInitializer.getSolutionsTopics();
    }

    private void initializeJmsClientObjects() throws JMSException {
        topicConnection = topicConnectionFactory.createTopicConnection();
        topicSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
        topicSubscribers = randomsSubscribers(topicSession);
    }

    private List<TopicSubscriber> randomsSubscribers(TopicSession topicSession) throws JMSException {
        List<TopicSubscriber> topicSubscribers = new ArrayList<>();

        int numberOfSubscriptions = new Random().nextInt(solutionsTopics.size()) + 1;
        for(int i=0; i<numberOfSubscriptions; i++) {
            int randInt = new Random().nextInt(solutionsTopics.size());
            topicSubscribers.add(topicSession.createSubscriber(solutionsTopics.get(randInt)));
            solutionsTopics.remove(randInt);
        }

        String topics = joinTopicNames(topicSubscribers);

        System.out.println(Thread.currentThread() + " subscribes " + topics);
        return topicSubscribers;
    }

    private String joinTopicNames(List<TopicSubscriber> topicSubscribers) throws JMSException {
        StringBuilder sb = new StringBuilder();
        for (TopicSubscriber topicSubscriber : topicSubscribers) {
            sb.append(topicSubscriber.getTopic().getTopicName());
            sb.append(", ");
        }

        sb.setCharAt(sb.length() - 1, '.');
        return sb.toString();
    }

    private void setMessageListeners(List<TopicSubscriber> topicSubscribers) throws JMSException {
        for(TopicSubscriber topicSubscriber : topicSubscribers) {
            topicSubscriber.setMessageListener(new TextMessageListener());
        }
    }

    public void start() throws JMSException, IOException {
        setMessageListeners(topicSubscribers);
        topicConnection.start();
        System.out.println("Connection started - receiving messages possible!");

        while (true) {
            System.out.println("Waiting for a message to arrive...");
            try {
                Thread.sleep(100000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void stop() {
        // close the connection
        if (topicConnection != null) {
            try {
                topicConnection.close();
            } catch (JMSException exception) {
                exception.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        ResultReader resultReader = null;
        try {
            resultReader = new ResultReader();
            resultReader.start();
        } catch (NamingException | JMSException | IOException e) {
            throw new RuntimeException(e);
        } finally {
            if(resultReader != null) {
                resultReader.stop();
            }
        }
    }
}
