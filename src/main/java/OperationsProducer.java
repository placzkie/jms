import javax.jms.*;
import javax.naming.NamingException;
import java.io.IOException;
import java.util.Random;

/**
 * Created by Leszek Placzkiewicz on 2016-04-07.
 */
public class OperationsProducer {
    private static final String OPERATOR_PROPERTY = "OPERATOR";
    private static final int SEND_INTERVAL = 3000;

    // JMS Administrative objects
    private QueueConnectionFactory queueConnectionFactory;
    private Queue operationsQueue;

    // JMS Client objects
    private QueueConnection connection;
    private QueueSession session;
    private QueueSender sender;

    private String producerName;

    private JMSInitializer jmsInitializer;

    public OperationsProducer() throws NamingException, JMSException {
        this("Producer " + new Random().nextInt(100));
    }

    public OperationsProducer(String producerName) throws NamingException, JMSException {
        this.producerName = producerName;
        this.jmsInitializer = JMSInitializer.getInstance();
        initializeAdministrativeObjects();
        initializeJmsClientObjects();
    }

    private void initializeAdministrativeObjects() throws NamingException {
        queueConnectionFactory = jmsInitializer.getQueueConnectionFactory();
        operationsQueue = jmsInitializer.getOperationsQueue();
    }

    private void initializeJmsClientObjects() throws JMSException {
        connection = queueConnectionFactory.createQueueConnection();
        session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        sender = session.createSender(operationsQueue);
    }

    public void start() throws JMSException, IOException {
        connection.start();

        while (true) {
            TextMessage message = session.createTextMessage();
            Operation operation = new Operation();
            message.setText(operation.toString());
            message.setStringProperty(OPERATOR_PROPERTY, operation.getOperator().toString());
            System.out.println(producerName + " produced: " + operation);
            sender.send(message);
            try {
                Thread.sleep(SEND_INTERVAL);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void stop() {
        if (connection != null) {
            try {
                connection.close();
            } catch (JMSException exception) {
                exception.printStackTrace();
            }
        }
    }
}
