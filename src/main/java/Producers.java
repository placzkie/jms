import javax.jms.JMSException;
import javax.naming.NamingException;
import java.io.IOException;

/**
 * Created by Leszek Placzkiewicz on 2016-04-07.
 */
public class Producers {
    private static final int DEFAULT_NUMBER_OF_PRODUCERS = 2;

    static void run() {
        run(DEFAULT_NUMBER_OF_PRODUCERS);
    }

    static void run(int numberOfProducers) {
        for(int i = 0; i< numberOfProducers; i++) {
            Thread t = new Thread(() -> {
                OperationsProducer tp = null;
                try {
                    tp = new OperationsProducer();
                    tp.start();
                } catch (NamingException | IOException | JMSException e) {
                    throw new RuntimeException(e);
                } finally {
                    if (tp != null) {
                        tp.stop();
                    }
                }
            });
            t.start();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        run();
    }
}
