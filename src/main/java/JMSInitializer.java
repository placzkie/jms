import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.jms.Topic;
import javax.jms.TopicConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Leszek Placzkiewicz on 2016-04-09.
 */
public class JMSInitializer {
    private static final String JNDI_CONTEXT_FACTORY_CLASS_NAME = "org.exolab.jms.jndi.InitialContextFactory";
    private static final String DEFAULT_JMS_PROVIDER_URL = "tcp://localhost:3035/";
    private static final String PLUS_TOPIC = "plusTopic";
    private static final String MINUS_TOPIC = "minusTopic";
    private static final String MULTIPLY_TOPIC = "multiplyTopic";
    private static final String DIVIDE_TOPIC = "divideTopic";

    private static final String DEFAULT_OPERATIONS_QUEUE_NAME = "queue1";

    // Application JNDI context
    private Context jndiContext;

    // JMS Administrative objects
    private QueueConnectionFactory queueConnectionFactory;
    private TopicConnectionFactory topicConnectionFactory;

    private Queue operationsQueue;
    private Topic plusTopic;
    private Topic minusTopic;
    private Topic multiplyTopic;
    private Topic divideTopic;

    private static JMSInitializer jmsInitializer;

    public static JMSInitializer getInstance() {
        if(jmsInitializer == null) {
            try {
                jmsInitializer = new JMSInitializer();
            } catch (NamingException e) {
                throw new RuntimeException(e);
            }
        }
        return jmsInitializer;
    }

    private JMSInitializer() throws NamingException {
        this(DEFAULT_JMS_PROVIDER_URL, DEFAULT_OPERATIONS_QUEUE_NAME, PLUS_TOPIC, MINUS_TOPIC, MULTIPLY_TOPIC, DIVIDE_TOPIC);
    }

    private JMSInitializer(String providerUrl,
                          String operationsQueueName,
                          String plusTopicName,
                          String minusTopicName,
                          String multiplyTopicName,
                          String divideTopicName) throws NamingException {
        initializeJndiContext(providerUrl);
        initializeAdministrativeObjects(operationsQueueName, plusTopicName, minusTopicName, multiplyTopicName, divideTopicName);
    }

    private void initializeJndiContext(String providerUrl) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_CONTEXT_FACTORY_CLASS_NAME);
        props.put(Context.PROVIDER_URL, providerUrl);
        jndiContext = new InitialContext(props);
    }

    private void initializeAdministrativeObjects(String operationsQueueName,
                                                 String plusTopicName,
                                                 String minusTopicName,
                                                 String multiplyTopicName,
                                                 String divideTopicName) throws NamingException {
        queueConnectionFactory = (QueueConnectionFactory) jndiContext.lookup("ConnectionFactory");
        operationsQueue = (Queue) jndiContext.lookup(operationsQueueName);

        topicConnectionFactory = (TopicConnectionFactory) jndiContext.lookup("ConnectionFactory");
        plusTopic = (Topic) jndiContext.lookup(plusTopicName);
        minusTopic = (Topic) jndiContext.lookup(minusTopicName);
        multiplyTopic = (Topic) jndiContext.lookup(multiplyTopicName);
        divideTopic = (Topic) jndiContext.lookup(divideTopicName);
    }

    public QueueConnectionFactory getQueueConnectionFactory() {
        return queueConnectionFactory;
    }

    public Queue getOperationsQueue() {
        return operationsQueue;
    }

    public TopicConnectionFactory getTopicConnectionFactory() {
        return topicConnectionFactory;
    }

    public Topic getPlusTopic() {
        return plusTopic;
    }

    public Topic getMinusTopic() {
        return minusTopic;
    }

    public Topic getMultiplyTopic() {
        return multiplyTopic;
    }

    public Topic getDivideTopic() {
        return divideTopic;
    }

    public List<Topic> getSolutionsTopics() {
        List<Topic> topics = new ArrayList<>();
        topics.add(plusTopic);
        topics.add(minusTopic);
        topics.add(multiplyTopic);
        topics.add(divideTopic);
        return topics;
    }
}
