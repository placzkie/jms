import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Created by Leszek Placzkiewicz on 2016-04-08.
 */
public class TextMessageListener implements MessageListener {
    private final String OPERATOR_PROPERTY = "OPERATOR";

    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            try {
                String operator = textMessage.getStringProperty(OPERATOR_PROPERTY);
                System.out.println(Thread.currentThread() + "Reading message: [" + operator + "] " + textMessage.getText());
            } catch (JMSException e) {
                System.out.println("JMSException");
                throw new RuntimeException(e);
            }
        }
    }
}
