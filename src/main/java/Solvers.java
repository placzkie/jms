import javax.jms.JMSException;
import javax.naming.NamingException;
import java.io.IOException;

/**
 * Created by Leszek Placzkiewicz on 2016-04-07.
 */
public class Solvers {
    private static final int NUMBER_OF_SOLVERS = 2;

    static void run() {
        for(int i = 0; i< NUMBER_OF_SOLVERS; i++) {
            Thread t = new Thread(() -> {
                OperationsSolver operationsSolver = null;
                try {
                    operationsSolver = new OperationsSolver();
                    operationsSolver.start();
                } catch (NamingException | JMSException | IOException e) {
                    throw new RuntimeException(e);
                } finally {
                    if(operationsSolver != null) {
                        operationsSolver.stop();
                    }
                }
            });
            t.start();
        }
    }

    public static void main(String[] args) {
        run();
    }
}
