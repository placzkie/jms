/**
 * Created by Leszek Placzkiewicz on 2016-04-09.
 */
public class Readers {
    private static final int DEFAULT_NUMBER_OF_READERS = 3;

    static void run() {
        run(DEFAULT_NUMBER_OF_READERS);
    }

    static void run(int numberOfReaders) {
        for(int i = 0; i< numberOfReaders; i++) {
            Thread t = new Thread(() -> {
                String[] p = new String[0];
                ResultReader.main(p);
            });
            t.start();
        }
    }

    public static void main(String[] args) {
        run();
    }
}
