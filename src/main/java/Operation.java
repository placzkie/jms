import java.util.Random;

/**
 * Created by Leszek Placzkiewicz on 2016-04-05.
 */
class Operation {
    enum Operator {
        PLUS("+"), MINUS("-"), MULTIPLY("*"), DIVIDE("/");

        private final String sign;

        Operator(String sign) {
            this.sign = sign;
        }

        @Override
        public String toString() {
            return sign;
        }
    }

    private final Operator operator;
    private final int a;
    private final int b;

    Operation(Operator operator, int a, int b) {
        this.operator = operator;
        this.a = a;
        this.b = b;
    }

    Operation() {
        this(generateRandomOperator(), generateRandomInt(), generateRandomInt());
    }

    Operator getOperator() {
        return operator;
    }

    @Override
    public String toString() {
        return String.format("%d%s%d", a, operator, b);
    }

    private static Operator generateRandomOperator() {
        Random r = new Random();
        int idx = r.nextInt(Operator.values().length);
        return Operator.values()[idx];
    }

    private static int generateRandomInt() {
        Random r = new Random();
        return r.nextInt(1000) + 1;
    }
}
