import javax.jms.*;
import javax.naming.NamingException;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.IOException;

/**
 * Created by Leszek Placzkiewicz on 2016-04-07.
 */
public class OperationsSolver {
    private static final String OPERATOR_PROPERTY = "OPERATOR";

    private JMSInitializer jmsInitializer;

    // JMS Administrative objects references
    private QueueConnectionFactory queueConnectionFactory;
    private TopicConnectionFactory topicConnectionFactory;
    private Queue operationsQueue;
    private Topic plusTopic;
    private Topic minusTopic;
    private Topic multiplyTopic;
    private Topic divideTopic;

    // JMS Client objects
    private QueueConnection queueConnection;
    private TopicConnection topicConnection;
    private QueueSession queueSession;
    private TopicSession topicSession;
    private QueueReceiver queueReceiver;
    private TopicPublisher topicPublisherPlus;
    private TopicPublisher topicPublisherMinus;
    private TopicPublisher topicPublisherMultiply;
    private TopicPublisher topicPublisherDivide;

    public OperationsSolver() throws NamingException, JMSException {
        jmsInitializer = JMSInitializer.getInstance();
        initializeAdministrativeObjects();
        initializeJmsClientObjects();
    }

    private void initializeAdministrativeObjects() throws NamingException {
        //Queue
        queueConnectionFactory = jmsInitializer.getQueueConnectionFactory();
        operationsQueue = jmsInitializer.getOperationsQueue();
        //Topics
        topicConnectionFactory = jmsInitializer.getTopicConnectionFactory();
        plusTopic = jmsInitializer.getPlusTopic();
        minusTopic = jmsInitializer.getMinusTopic();
        multiplyTopic = jmsInitializer.getMultiplyTopic();
        divideTopic = jmsInitializer.getDivideTopic();
    }

    private void initializeJmsClientObjects() throws JMSException {
        //Queue
        queueConnection = queueConnectionFactory.createQueueConnection();
        queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        queueReceiver = queueSession.createReceiver(operationsQueue);
        //Topics
        topicConnection = topicConnectionFactory.createTopicConnection();
        topicSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
        topicPublisherPlus = topicSession.createPublisher(plusTopic);
        topicPublisherMinus = topicSession.createPublisher(minusTopic);
        topicPublisherMultiply = topicSession.createPublisher(multiplyTopic);
        topicPublisherDivide = topicSession.createPublisher(divideTopic);
    }

    public void start() throws JMSException, IOException {
        queueConnection.start();
        System.out.println("Connection started - receiving messages possible!");

        while (true) {
            Message message = queueReceiver.receive();
            if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                String operator = textMessage.getStringProperty(OPERATOR_PROPERTY);
                double result = calculate(textMessage.getText());
                System.out.println(Thread.currentThread() + " Reading message: " +
                        textMessage.getText() + "." + "Result: " + result);

                publishResultMessage(result, operator);
            }
        }
    }
    
    private void publishResultMessage(double result, String operator) throws JMSException {
        TextMessage message = topicSession.createTextMessage();
        message.setText(String.valueOf(result));
        message.setStringProperty(OPERATOR_PROPERTY, operator);

        switch(operator) {
            case "+":
                topicPublisherPlus.publish(message);
                break;
            case "-":
                topicPublisherMinus.publish(message);
                break;
            case "*":
                topicPublisherMultiply.publish(message);
                break;
            case "/":
                topicPublisherDivide.publish(message);
                break;
        }
    }

    private double calculate(String message) {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");
        double result;
        try {
            result = Double.valueOf(engine.eval(message).toString());
        } catch (ScriptException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public void stop() {
        if (queueConnection != null) {
            try {
                queueConnection.close();
            } catch (JMSException exception) {
                exception.printStackTrace();
            }
        }
    }
}
